﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using tp2_5.Model;
using tp2_5.Providers;
using System.Data;
using System.Data.Common;


namespace tp2_5.Services
{
    public class PostServices
    {
        public static DataTable GetAll()
        {
            return PostSql.GetPostsDataTable();
        }
    }
}