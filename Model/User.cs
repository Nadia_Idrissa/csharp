﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tp2_5.Model
{
    public class User
    {
        public User()
        {

        }
        public User(string courriel,string mdp)
        {
            this.Courriel = courriel;
            this.Mdp = mdp;
        }
        public string Courriel { get; set; }
        public string Mdp { get; set; }
    }
}