﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace tp2_5.Scripts
{
    public class Posts
    {
        public Posts()
        {

        }
        public  Posts(string contenu, string iduser, int likes)
        {
            this.Contenu = contenu;
            this.IdUser = iduser;
            this.Likes = likes;
        }

        public string Contenu { get; set; }
        public string IdUser { get; set; }
        public int Likes { get; set; }
    }

}