﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Common;
using MySql.Data.MySqlClient;
namespace tp2_5.Providers
{
    public class PostSql
    {
        private static readonly string ConnectionString = "Server=127.0.0.1;Uid=root;Pwd=root;Database=posts;";
        private static DbConnection cnx;
        private static DbCommand cmd;
        private static DbDataReader reader;

        public static DataTable GetPostsDataTable()
        {
            cnx = new MySqlConnection
            {
                ConnectionString = ConnectionString
            };
            cnx.Open();

            cmd = cnx.CreateCommand();
            cmd.CommandType = CommandType.TableDirect;
            cmd.CommandText = "post";
            DbDataAdapter dataAdapter = new MySqlDataAdapter((MySqlCommand)cmd);

            DataTable dtPosts = new DataTable();
            dataAdapter.Fill(dtPosts);
            return dtPosts;
        }
    }
}