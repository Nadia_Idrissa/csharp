﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using MySql.Data.MySqlClient;
using System.Data;
using tp2_5.Services;
using tp2_5.Model;

namespace tp2_5
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            System.Data.DataTable dt = PostServices.GetAll();
            TableHeaderRow entete = new TableHeaderRow();
            TableCell cellule;
            HyperLink lien;

            for (int j = 0; j < dt.Columns.Count; j++)
            {
                cellule = new TableCell();
                cellule.Text = dt.Columns[j].ToString();
                entete.Cells.Add(cellule);
            }

            postsTable.Rows.Add(entete);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                TableRow ligne = new TableRow();
                for (int n = 0; n < dt.Columns.Count; n++)
                {
                    cellule = new TableCell();
                    cellule.Text = dt.Rows[i][n].ToString();
                    ligne.Cells.Add(cellule);
                }
                cellule = new TableCell();
                lien = new HyperLink();
                lien.NavigateUrl = "&id" + dt.Rows[i]["ID"].ToString();
                //cellule.Text = "ajouter au panier";
                cellule.Controls.Add(lien);
                ligne.Cells.Add(cellule);
                postsTable.Rows.Add(ligne);

            }

        }
      

    }
}